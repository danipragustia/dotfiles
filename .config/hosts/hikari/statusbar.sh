#!/bin/sh

current_time() {
    LOCAL=$(date +"%d-%m-%Y %l:%M:%S %p")
    CEST=$(TZ=Europe/Amsterdam date +"%I:%M %p")
    echo " (CEST ${CEST}) ${LOCAL}"
}

fetch_attr_or_empty() {
    echo "${METADATA}" | jq ".data.\"$1\"" | sed -e 's/\n/ /g; s/\s*$//g' | tr -d '"'
}

mpv_current_playing() {
    SOCKET=$HOME/mpv-socket
    METADATA=$(echo '{ "command": ["get_property", "filtered-metadata"] }' | socat - $SOCKET)
    ICY_TITLE="$(fetch_attr_or_empty icy-title)"
    if [ -z "${ICY_TITLE}" ]; then
	TITLE="$(fetch_attr_or_empty Title)"
	ARTIST="$(fetch_attr_or_empty Artist)"
	ALBUM="$(fetch_attr_or_empty Album)"

	DESCRIPTION=''
	if [ -n "$TITLE" ]; then
	    DESCRIPTION="${TITLE}"
	fi
	if [ -n "$ARTIST" ]; then
	    DESCRIPTION="${DESCRIPTION} - ${ARTIST}"
	fi
	if [ -n "$ALBUM" ]; then
	    DESCRIPTION="${DESCRIPTION} (${ALBUM})"
	fi

	if [ -z "${DESCRIPTION}" ]; then
	    echo ""
	else
	    echo "♪ ${DESCRIPTION}"
	fi
    else
	echo "♪ ${ICY_TITLE}"
    fi
}

echo "$(mpv_current_playing) $(current_time)"
