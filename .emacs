;;; Packages
(require 'package)
(add-to-list 'package-archives
	     '("melpa" . "http://melpa.org/packages/") t)
(package-initialize)
(when (not package-archive-contents)
  (package-refresh-contents))
(defvar packages
  '(multiple-cursors
    lsp-mode
    gruber-darker-theme
    smex
    web-mode
    markdown-mode
    go-mode
    )
  )
(mapc #'(lambda (package)
	  (unless (package-installed-p package)
	    (package-install package)))
      packages)

;;; Emacs Basic Customize
(tool-bar-mode 0)
(menu-bar-mode 0)
(scroll-bar-mode 0)
(column-number-mode 1)
(show-paren-mode 1)

(add-to-list 'default-frame-alist '(fullscreen . maximized))
;; (add-to-list 'default-frame-alist
;;              '(font . "CozetteVector-10"))

(global-display-line-numbers-mode)

;;; Global Variable
(setq custom-file "~/.emacs.custom.el")
(setq auto-save-default nil)
(setq create-lockfiles nil)
(setq inhibit-startup-screen t)
(setq backup-directory-alist '(("" . "~/.emacs.d/backup")))
(setq x-select-enable-clipboard t)

(ido-mode 1)

;;; Load Theme
(load-theme 'gruber-darker t)

;;; Mutiple Cursors
(require 'multiple-cursors)
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
(global-set-key (kbd "C-,") 'mc/mark-next-like-this)
(global-set-key (kbd "C-.") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c ,") 'mc/mark-all-like-this)

;;; Whitespace
(require 'whitespace)
(setq whitespace-style '(face empty tabs lines-tail trailing))
(global-whitespace-mode t)

;;; Indentation
(setq-default tab-width 4)
(setq-default indent-tabs-mode nil)
(setq-default c-basic-offset 4)

;;; 80 Column Ruler
(setq fill-column 80)
(add-hook 'prog-mode-hook #'display-fill-column-indicator-mode)

;;; Unixism
(defun no-junk-please-were-unixish ()
  (let ((coding-str (symbol-name buffer-file-coding-system)))
    (when (string-match "-\\(?:dos\\|mac\\)$" coding-str)
      (set-buffer-file-coding-system 'unix))))
(add-hook 'find-file-hooks 'no-junk-please-were-unixish)

;;; Smex
(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "C-c M-x") 'execute-extended-command)

;;; Go
(require 'lsp-mode)
(add-hook 'go-mode-hook #'lsp-deferred)

;;; Company Mode
(add-hook 'after-init-hook 'global-company-mode)

;; Set up before-save hooks to format buffer and add/delete imports.
;; Make sure you don't have other gofmt/goimports hooks enabled.
(defun lsp-go-install-save-hooks ()
  (add-hook 'before-save-hook #'lsp-format-buffer t t)
  (add-hook 'before-save-hook #'lsp-organize-imports t t))
(add-hook 'go-mode-hook #'lsp-go-install-save-hooks)

(add-to-list 'auto-mode-alist '("\\.js\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.css\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.go\\'" . go-mode))
