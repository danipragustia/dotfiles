ENV=$HOME/.shinit; export ENV

export PATH=$PATH:~/bin
export EDITOR=emacs
export LANG=en_US.UTF-8
export GOPATH=~/.local/share/go
export GDK_BACKEND=wayland
export SDL_VIDEODRIVER=wayland
export HISTSIZE=65536

export GOPROXY=direct
export GOSUMDB=off
export GOTELEMETRY=off
export GOTOOLCHAIN=local\
